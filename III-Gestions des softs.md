# Tp1


### III.Gestion de softs : 

###  Expliquer l'intérêt de l'utilisation d'un gestionnaire de paquets : 

##### - Un gestionnaire de paquets est un logiciel, permettant l'automatisation de mise a jour, installation, désinstallation d'autres logiciel sur un ordinateur. Ou simplement le téléchargement de paquets.                      Les gestionnaires de paquets permettant la localisation et l'installation de paquets ainsi que de mise a jour plus facilement que lors d'une recherche internet                  
### Utiliser un gestionnaire de paquet propres à votre OS : 

#### Liste des paquets installer
```
PS C:\Users\adrie> choco list -l
Chocolatey v0.10.15
2 validations performed. 1 success(es), 1 warning(s), and 0 error(s).

Validation Warnings:
 - A pending system reboot request has been detected, however, this is
   being ignored due to the current command being used 'list'.
   It is recommended that you reboot at your earliest convenience.

chocolatey 0.10.15
1 packages installed.
```
#### Provenance des paquets
```
PS C:\Windows\system32> choco source list
Chocolatey v0.10.15
2 validations performed. 1 success(es), 1 warning(s), and 0 error(s).

Validation Warnings:
 - A pending system reboot request has been detected, however, this is
   being ignored due to the current command being used 'source'.
   It is recommended that you reboot at your earliest convenience.

chocolatey - https://chocolatey.org/api/v2/ | Priority 0|Bypass Proxy - False|Self-Service - False|Admin Only - False.
```
