﻿#script qui permet d'afficher un résumé de l'os
#Auteur : Clamadieu-Tharaud Adrien
#Date : 19/10/2020

Write-Output "Nom de l'ordinateur : $env:computername"
$OS = (Get-WMIObject win32_operatingsystem).name
$OS = $OS.split("|")[0]
Write-Output "OS : $OS"
$OSVersion = (Get-WMIObject win32_operatingsystem).version
Write-Output "OS Version : $OSVersion"
$DateEtHeure = (Get-CimInstance Win32_OperatingSystem).LastBootUpTime 
Write-Output "Date et heure d'allumage : $DateEtHeure"
$criteria = "Type='software' and IsAssigned=1 and IsHidden=0 and IsInstalled=0"
$searcher = (New-Object -COM Microsoft.Update.Session).CreateUpdateSearcher()
$updates = $searcher.Search($criteria).Updates
if ($updates.Count -ne 0) {
    $osUpdated = "Le systeme est t'il a jour : False"
}
else {
    $osUpdated = "Le systeme est t'il a jour : True)"
}
Write-Output "est-ce que l'os est a jour : $osUpdated"

Write-Output " "

$RamTotale=[STRING]((Get-WmiObject -Class Win32_ComputerSystem ).TotalPhysicalMemory/1GB)
Write-Output "Ram totale : $RamTotale"
$RamLibre = (Get-CIMInstance Win32_OperatingSystem).FreePhysicalMemory
Write-Output "RAM disponible : $RamLibre Go"
$RamUtiliser=[String]((Get-WmiObject -Class Win32_OperatingSystem).FreePhysicalMemory/1MB)
Write-Output "Ram utiliser : $RamUtiliser Go"
$DiskDispo = [Math]::Round((Get-Volume -DriveLetter 'C').Size/1GB)
Write-Output "Espace disque disponible : $DiskDispo Go"
$DiskUtiliser = [Math]::Round((Get-Volume -DriveLetter 'C').SizeRemaining/1GB)
Write-Output "Espace disque utiliser : $DiskUtiliser Go"

Write-Output " "

$DownloadURL = "https://bintray.com/ookla/download/download_file?file_path=ookla-speedtest-1.0.0-win64.zip"
$DownloadLocation = "$($Env:ProgramData)\SpeedtestCLI"
try {
    $TestDownloadLocation = Test-Path $DownloadLocation
    if (!$TestDownloadLocation) {
        new-item $DownloadLocation -ItemType Directory -force
        Invoke-WebRequest -Uri $DownloadURL -OutFile "$($DownloadLocation)\speedtest.zip"
        Expand-Archive "$($DownloadLocation)\speedtest.zip" -DestinationPath $DownloadLocation -Force
    } 
}
catch {  
    write-host "The download and extraction of SpeedtestCLI failed. Error: $($_.Exception.Message)"
    exit 1
}
$SpeedtestResults = & "$($DownloadLocation)\speedtest.exe" --format=json --accept-license --accept-gdpr
$SpeedtestResults = $SpeedtestResults | ConvertFrom-Json

$ip = (Test-Connection -ComputerName $env:computername -count 1).IPV4Address.ipaddressTOstring
Write-Output "Ip principale : $ip"
$connection = (Test-Connection -ComputerName "8.8.8.8" -Count 4  | measure-Object -Property ResponseTime -Average).average
Write-Output " - Ping : $connection ms"
$downloadspeed = [math]::Round($SpeedtestResults.download.bandwidth / 1000000 * 8, 2)
$uploadspeed = [math]::Round($SpeedtestResults.upload.bandwidth / 1000000 * 8, 2)
Write-Output " - download speed : $downloadSpeed Mbit/s"
Write-Output " - upload speed : $uploadspeed Mbit/s"

Write-Output " "

$Users = (Get-WmiObject Win32_UserAccount).Name 
Write-Output "La liste des utilisateurs : $Users"






