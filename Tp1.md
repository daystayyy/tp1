# Tp1


### Host os :

#### command : systeminfo
```
PS C:\Users\adrie> systeminfo

Nom de l’hôte:                              LAPTOP-537Q5UAV
Nom du système d’exploitation:              Microsoft Windows 10 Famille
Version du système:                         10.0.18363 N/A version 18363
[...]
Processeur(s):                              1 processeur(s) installé(s).
                                            [01] : AMD64 Family 23 Model 24 Stepping 1 AuthenticAMD ~2100 MHz
[...]
Mémoire physique totale:                    7 104 Mo
[...]
```

#### command : set
```
C:\Users\adrie>set

[...]
PROCESSOR_ARCHITECTURE=AMD64
[...]
```

#### command : wmic MemoryChip
```
PS C:\Users\adrie> wmic MemoryChip

[...]
Physical Memory                        HMA851S6CJR6N-VK
[...]
Physical Memory                        HMA851S6CJR6N-VK 
[...]
```


### Devices
#### command : WMIC CPU Get 
PS C:\Users\adrie> WMIC CPU Get 
```
DeviceID,NumberOfCores,NumberOfLogicalProcessors

DeviceID  NumberOfCores  NumberOfLogicalProcessors
CPU0      4              8
```

#### command : systeminfo
```
PS C:\Users\adrie> systeminfo


[...]
Processeur(s):                              1 processeur(s) installé(s).
                                            [01] : AMD64 Family 23 Model 24 Stepping 1 AuthenticAMD ~2100 MHz
[...]
```

#### command : diskpart
```
PS C:\Users\adrie> diskpart
```
```
DISKPART>      

  N° disque  Statut         Taille   Libre    Dyn  GPT
  ---------  -------------  -------  -------  ---  ---
  Disque 0    En ligne        238 G octets      0 octets        *

DISKPART> select disk 0

Le disque 0 est maintenant le disque sélectionné.

DISKPART> detail disk

WDC PC SN730 SDBPNTY-256G-1027
ID du disque : {A8BC463E-1C97-4056-8FAA-43D0C3EC201B}
Type : NVMe


  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
  Volume 0     C   Windows      NTFS   Partition     80 G   Sain       Démarrag
  Volume 1     D   Data         NTFS   Partition    142 G   Sain
  Volume 2         SYSTEM       FAT32  Partition    100 M   Sain       Système
  
DISKPART> list partition

  N° partition   Type              Taille   Décalage
  -------------  ----------------  -------  --------
  Partition 1    Système            100 M   1024 K
  Partition 2    Réservé             16 M    101 M
  Partition 3    Principale          80 G    117 M
  Partition 4    Principale         142 G     80 G
  Partition 5    Récupération       512 M    222 G
  Partition 6    Récupération        14 G    223 G
  Partition 7    Récupération      1024 M    237 G
```
 
 ```    
  DISKPART> select partition 1

La partition 1 est maintenant la partition sélectionnée.
 
 DISKPART> detail partition
 
 [...]
 N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
* Volume 2         SYSTEM       FAT32  Partition    100 M   Sain       Système
```
Partition 2 : 
```
Il n’y a pas de volume associé avec cette partition.
```
Partition 3 : 
```
[...] FS
      --
      NTFS [...]
```
Partition 4 : 
```
[...] FS
      --
      NTFS [...]
```
Partition 5 : 
```
[...] FS
      --
      FAT32 [...]
```
Partition 6 : 
```
[...] FS
      --
      NTFS [...]
```
Partition 7 : 
```
[...] FS
      --
      NTFS [...]
```
#### Les fonctions des partitions : 

La partition type systeme : stocks des fichiers systeme

La partition type réservé : est créé pour réserver une partie de l'espace disque en vue d'une par un système d'exploitation

Les partitions type principale : Stocks l'os ainsi que les fichiers utilisateurs

Las partitions récupération : stocks les donné nécessaire a une récupération/réparation/réintilialisation de l'os

### Users


```
C:\Users\adrie>wmic useraccount list full


AccountType=512
Description=Compte d'utilisateur d'administration
Disabled=TRUE
Domain=LAPTOP-537Q5UAV
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=Administrateur
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-2825419024-1735013321-1474895746-500
SIDType=1
Status=Degraded


AccountType=512
Description=
Disabled=FALSE
Domain=LAPTOP-537Q5UAV
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=adrie
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-2825419024-1735013321-1474895746-1001
SIDType=1
Status=OK


AccountType=512
Description=Compte utilisateur géré par le système.
Disabled=TRUE
Domain=LAPTOP-537Q5UAV
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=DefaultAccount
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=FALSE
SID=S-1-5-21-2825419024-1735013321-1474895746-503
SIDType=1
Status=Degraded


AccountType=512
Description=Compte d'utilisateur invité
Disabled=TRUE
Domain=LAPTOP-537Q5UAV
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=Invité
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=FALSE
SID=S-1-5-21-2825419024-1735013321-1474895746-501
SIDType=1
Status=Degraded


AccountType=512
Description=Compte d'utilisateur géré et utilisé par le système pour les scénarios Windows Defender Application Guard.
Disabled=TRUE
Domain=LAPTOP-537Q5UAV
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=WDAGUtilityAccount
PasswordChangeable=TRUE
PasswordExpires=TRUE
PasswordRequired=TRUE
SID=S-1-5-21-2825419024-1735013321-1474895746-504
SIDType=1
Status=Degraded
```

#### Processus

```

C:\Users\adrie>tasklist

Nom de l’image                 PID Nom de la sessio Numéro de s Utilisation
========================= ======== ================ =========== ============
System Idle Process              0 Services                   0         8 Ko
System                           4 Services                   0        20 Ko
Registry                       120 Services                   0    57 692 Ko
smss.exe                       936 Services                   0       556 Ko
csrss.exe                     1032 Services                   0     2 520 Ko
wininit.exe                   1140 Services                   0     2 772 Ko
services.exe                  1284 Services                   0     7 228 Ko
lsass.exe                     1300 Services                   0    17 472 Ko
svchost.exe                   1436 Services                   0     1 252 Ko
fontdrvhost.exe               1468 Services                   0     2 088 Ko
svchost.exe                   1484 Services                   0    41 448 Ko
WUDFHost.exe                  1556 Services                   0     2 508 Ko
svchost.exe                   1644 Services                   0    15 556 Ko
svchost.exe                   1704 Services                   0     5 128 Ko
svchost.exe                   1884 Services                   0     2 304 Ko
svchost.exe                   1904 Services                   0     4 100 Ko
svchost.exe                   1916 Services                   0     7 964 Ko
svchost.exe                   2036 Services                   0     2 376 Ko
svchost.exe                   1056 Services                   0     5 312 Ko
svchost.exe                   1020 Services                   0     5 012 Ko
svchost.exe                   2140 Services                   0     9 260 Ko
svchost.exe                   2192 Services                   0     4 508 Ko
svchost.exe                   2200 Services                   0     6 968 Ko
svchost.exe                   2284 Services                   0    10 620 Ko
svchost.exe                   2360 Services                   0     3 336 Ko
svchost.exe                   2444 Services                   0     7 052 Ko
svchost.exe                   2644 Services                   0     6 920 Ko
svchost.exe                   2664 Services                   0     3 036 Ko
svchost.exe                   2688 Services                   0     4 044 Ko
svchost.exe                   2696 Services                   0     6 456 Ko
svchost.exe                   2704 Services                   0    15 180 Ko
WUDFHost.exe                  2740 Services                   0     3 896 Ko
svchost.exe                   2856 Services                   0     3 880 Ko
svchost.exe                   2940 Services                   0     9 044 Ko
svchost.exe                   2948 Services                   0     5 024 Ko
svchost.exe                   2480 Services                   0     6 872 Ko
svchost.exe                   3136 Services                   0     5 916 Ko
svchost.exe                   3248 Services                   0     1 796 Ko
svchost.exe                   3336 Services                   0     4 120 Ko
svchost.exe                   3496 Services                   0     6 724 Ko
atiesrxx.exe                  3588 Services                   0     1 796 Ko
svchost.exe                   3624 Services                   0     1 788 Ko
svchost.exe                   3632 Services                   0     6 800 Ko
svchost.exe                   3640 Services                   0     3 652 Ko
Memory Compression            3732 Services                   0   183 600 Ko
svchost.exe                   3772 Services                   0     4 392 Ko
svchost.exe                   3800 Services                   0     3 844 Ko
svchost.exe                   3888 Services                   0     9 816 Ko
svchost.exe                   4032 Services                   0     2 812 Ko
svchost.exe                   4040 Services                   0     5 368 Ko
svchost.exe                   2824 Services                   0    12 424 Ko
svchost.exe                   3820 Services                   0     7 924 Ko
spoolsv.exe                   4120 Services                   0    10 044 Ko
svchost.exe                   4172 Services                   0     7 936 Ko
svchost.exe                   4208 Services                   0    18 056 Ko
svchost.exe                   4256 Services                   0     2 652 Ko
svchost.exe                   4496 Services                   0     3 156 Ko
svchost.exe                   4572 Services                   0    21 824 Ko
AdobeUpdateService.exe        4588 Services                   0     3 072 Ko
DAX3API.exe                   4596 Services                   0     5 320 Ko
LCD_Service.exe               4604 Services                   0     7 148 Ko
FMService64.exe               4612 Services                   0     2 844 Ko
svchost.exe                   4620 Services                   0    42 608 Ko
MateBookService.exe           4656 Services                   0    11 492 Ko
svchost.exe                   4676 Services                   0    13 812 Ko
svchost.exe                   4684 Services                   0    21 104 Ko
svchost.exe                   4872 Services                   0     3 612 Ko
RtkAudUService64.exe          4916 Services                   0     4 496 Ko
SessionService.exe            4952 Services                   0     1 004 Ko
RtkBtManServ.exe              4960 Services                   0     3 304 Ko
svchost.exe                   4988 Services                   0     1 860 Ko
svchost.exe                   4996 Services                   0     3 788 Ko
svchost.exe                   5076 Services                   0    13 912 Ko
svchost.exe                   4356 Services                   0     1 532 Ko
svchost.exe                   5192 Services                   0    11 624 Ko
svchost.exe                   5200 Services                   0     1 920 Ko
svchost.exe                   5908 Services                   0     4 888 Ko
unsecapp.exe                  7048 Services                   0     4 204 Ko
WmiPrvSE.exe                  7096 Services                   0     6 472 Ko
svchost.exe                   7696 Services                   0     6 132 Ko
svchost.exe                   8104 Services                   0    14 548 Ko
svchost.exe                   8224 Services                   0     3 232 Ko
svchost.exe                   9112 Services                   0    15 392 Ko
svchost.exe                   8236 Services                   0     2 704 Ko
svchost.exe                   9340 Services                   0    11 016 Ko
GoogleCrashHandler.exe        8812 Services                   0     1 316 Ko
GoogleCrashHandler64.exe      9544 Services                   0       140 Ko
dllhost.exe                  11020 Services                   0     4 460 Ko
svchost.exe                  11500 Services                   0     6 892 Ko
svchost.exe                  11412 Services                   0     4 068 Ko
SecurityHealthService.exe    13136 Services                   0     8 932 Ko
svchost.exe                  11420 Services                   0     4 748 Ko
svchost.exe                  13812 Services                   0     9 484 Ko
svchost.exe                   8096 Services                   0    14 052 Ko
svchost.exe                  11340 Services                   0    12 752 Ko
svchost.exe                  14536 Services                   0     6 640 Ko
SgrmBroker.exe                3364 Services                   0     4 696 Ko
svchost.exe                   6728 Services                   0    10 440 Ko
svchost.exe                   9988 Services                   0    16 860 Ko
svchost.exe                   4936 Services                   0     8 676 Ko
RvControlSvc.exe             13888 Services                   0     7 244 Ko
svchost.exe                  13256 Services                   0     3 544 Ko
OfficeClickToRun.exe         16620 Services                   0    36 056 Ko
AppVShNotify.exe             16392 Services                   0     2 128 Ko
svchost.exe                   8552 Services                   0     2 492 Ko
svchost.exe                  15952 Services                   0     2 152 Ko
svchost.exe                  11828 Services                   0     6 068 Ko
svchost.exe                   3060 Services                   0     6 228 Ko
MsMpEng.exe                  12552 Services                   0   232 824 Ko
NisSrv.exe                   12200 Services                   0    12 504 Ko
svchost.exe                   3456 Services                   0     9 544 Ko
servicehost.exe              14960 Services                   0    23 684 Ko
AGSService.exe                8844 Services                   0     9 188 Ko
vds.exe                       5756 Services                   0    11 368 Ko
svchost.exe                  17052 Services                   0    15 892 Ko
csrss.exe                    16380 Console                   22     5 268 Ko
winlogon.exe                 14128 Console                   22     9 604 Ko
fontdrvhost.exe              12388 Console                   22     5 872 Ko
dwm.exe                      15608 Console                   22    62 772 Ko
atieclxx.exe                 12656 Console                   22     9 744 Ko
DAX3API.exe                   4184 Console                   22     9 376 Ko
svchost.exe                  18976 Services                   0     5 012 Ko
uihost.exe                   16656 Console                   22    16 276 Ko
RtkAudUService64.exe         14788 Console                   22     7 412 Ko
sihost.exe                   11696 Console                   22    26 864 Ko
svchost.exe                   6440 Console                   22    29 816 Ko
svchost.exe                  11456 Console                   22    28 828 Ko
MBAMessageCenter.exe         17132 Console                   22    56 120 Ko
taskhostw.exe                17376 Console                   22    14 712 Ko
explorer.exe                 16580 Console                   22   136 180 Ko
taskhostw.exe                14364 Console                   22     1 456 Ko
svchost.exe                   9976 Console                   22    31 432 Ko
ctfmon.exe                   11560 Console                   22    15 584 Ko
StartMenuExperienceHost.e    10948 Console                   22    59 700 Ko
RuntimeBroker.exe            11892 Console                   22    28 304 Ko
WindowsInternal.Composabl    16868 Console                   22    55 412 Ko
SearchUI.exe                  7760 Console                   22    89 164 Ko
RuntimeBroker.exe            11316 Console                   22    38 028 Ko
SettingSyncHost.exe          11484 Console                   22     8 100 Ko
YourPhone.exe                 4400 Console                   22    16 056 Ko
RuntimeBroker.exe             9852 Console                   22    17 432 Ko
RuntimeBroker.exe             6804 Console                   22    19 784 Ko
SecurityHealthSystray.exe     3864 Console                   22     8 012 Ko
RtkAudUService64.exe         16472 Console                   22     9 580 Ko
EpicGamesLauncher.exe         6988 Console                   22    64 560 Ko
UnrealCEFSubProcess.exe       1852 Console                   22    27 792 Ko
CCXProcess.exe               14460 Console                   22     2 080 Ko
node.exe                     16556 Console                   22    55 344 Ko
conhost.exe                    832 Console                   22     5 436 Ko
AdobeIPCBroker.exe           16224 Console                   22     9 200 Ko
MonitorManageStart.exe       16548 Console                   22    20 892 Ko
ApplicationFrameHost.exe      4772 Console                   22    34 012 Ko
WinStore.App.exe              1248 Console                   22       224 Ko
RuntimeBroker.exe            17100 Console                   22    17 644 Ko
SystemSettings.exe           10548 Console                   22       244 Ko
PaintStudio.View.exe         12188 Console                   22       384 Ko
RuntimeBroker.exe            16168 Console                   22     9 512 Ko
Calculator.exe                3180 Console                   22        40 Ko
Video.UI.exe                  3324 Console                   22        44 Ko
svchost.exe                  17256 Console                   22    11 732 Ko
AppVShNotify.exe             10104 Console                   22     7 956 Ko
SearchIndexer.exe             9068 Services                   0    33 632 Ko
LockApp.exe                   9944 Console                   22    38 168 Ko
RuntimeBroker.exe            13872 Console                   22    25 456 Ko
osdservice.exe               15116 Services                   0    11 188 Ko
ShellExperienceHost.exe       2720 Console                   22    38 816 Ko
RuntimeBroker.exe            18896 Console                   22    14 800 Ko
chrome.exe                   10372 Console                   22   277 296 Ko
chrome.exe                    6236 Console                   22     6 276 Ko
chrome.exe                    6736 Console                   22   212 272 Ko
chrome.exe                    5224 Console                   22    59 912 Ko
chrome.exe                    5816 Console                   22    58 096 Ko
chrome.exe                   17048 Console                   22   109 768 Ko
chrome.exe                   18120 Console                   22    60 624 Ko
chrome.exe                   15056 Console                   22    71 804 Ko
chrome.exe                    6684 Console                   22    94 208 Ko
chrome.exe                    6956 Console                   22   112 044 Ko
chrome.exe                    4444 Console                   22   158 560 Ko
chrome.exe                    1424 Console                   22    37 524 Ko
Discord.exe                   9440 Console                   22    65 240 Ko
Discord.exe                   9552 Console                   22    94 264 Ko
Discord.exe                  10264 Console                   22    23 984 Ko
Discord.exe                   3464 Console                   22    11 132 Ko
Discord.exe                  16424 Console                   22   221 772 Ko
Discord.exe                   7860 Console                   22    16 108 Ko
chrome.exe                   14348 Console                   22   112 420 Ko
Code.exe                      2268 Console                   22    13 568 Ko
Code.exe                      6416 Console                   22    30 072 Ko
dllhost.exe                   7776 Console                   22    13 096 Ko
cmd.exe                       8404 Console                   22    12 916 Ko
conhost.exe                   7936 Console                   22    13 636 Ko
chrome.exe                    3032 Console                   22    39 884 Ko
chrome.exe                   17040 Console                   22   105 796 Ko
chrome.exe                   15016 Console                   22   312 696 Ko
chrome.exe                   17204 Console                   22    52 900 Ko
chrome.exe                   16192 Console                   22    54 804 Ko
chrome.exe                    2984 Console                   22    93 628 Ko
chrome.exe                   18392 Console                   22   133 812 Ko
chrome.exe                   12548 Console                   22    21 820 Ko
chrome.exe                    2064 Console                   22    43 024 Ko
tasklist.exe                 18992 Console                   22     8 440 Ko
WmiPrvSE.exe                  7076 Services                   0     8 868 Ko
```

```
C:\Users\adrie>tasklist

[...]
csrss.exe                     1032 Services                   0     2 520 Ko
wininit.exe                   1140 Services                   0     2 772 Ko
svchost.exe                   1436 Services                   0     1 252 Ko
fontdrvhost.exe   
[...] 
```
csrss.exe est le processus de Windows servant à gérer les fenêtres et les éléments graphiques de Windows.

wininit.exe est le processus d'initialistion de Windows

svchost.exe sert d'hôte pour les fonctionnalités de bibliothèques de liens dynamiques

fontdrvhost.exe gère les pilotes de police dans le compte d'utilisateur actuel

lsass.exe il assure l'identification des utilisateurs

#### Network

```
C:\Users\adrie>ipconfig

Configuration IP de Windows


Carte Ethernet Radmin VPN :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6. . . . . . . . . . . . . .: fdfd::1aa4:dfec
   Adresse IPv6 de liaison locale. . . . .: fe80::2ca6:1b53:a9c8:6aa4%10
   Adresse IPv4. . . . . . . . . . . . . .: 26.164.223.236
   Masque de sous-réseau. . . . . . . . . : 255.0.0.0
   Passerelle par défaut. . . . . . . . . : 26.0.0.1

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::d83c:abf5:8608:992b%8
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.50
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . : 192.168.1.1

Carte Ethernet Connexion réseau Bluetooth :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :

Carte Tunnel Teredo Tunneling Pseudo-Interface :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6. . . . . . . . . . . . . .: 2001:0:1428:8f18:411:3f65:b27c:21e5
   Adresse IPv6 de liaison locale. . . . .: fe80::411:3f65:b27c:21e5%12
   Passerelle par défaut. . . . . . . . . :
```
Carte Ethernet est une carte qui met a dispotition un ou plusieurs port ethernet RJ45 (Ethernet)

Carte réseau sans fil Wi-Fi permet une connection en wi-fi

Carte Ethernet Connexion réseau Bluetooth permet une connection en Bluetooth

Carte Tunnel Teredo Tunneling Pseudo-Interface est une carte permettant d'encapsuler des paquets IPv6 dans des paquets IPv4 lorsque les périphériques réseau ne supportent pas la norme IPv6

```
C:\Users\adrie>Netstat

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    192.168.1.50:61474     40.67.254.36:https     ESTABLISHED
  TCP    192.168.1.50:61507     ec2-34-196-127-242:https  ESTABLISHED
  TCP    192.168.1.50:61554     a2-16-210-111:https    CLOSE_WAIT
  TCP    192.168.1.50:61555     a2-16-210-111:https    CLOSE_WAIT
  TCP    192.168.1.50:61556     a2-16-210-111:https    CLOSE_WAIT
  TCP    192.168.1.50:61557     a2-16-210-111:https    CLOSE_WAIT
  TCP    192.168.1.50:61558     a2-16-210-111:https    CLOSE_WAIT
  TCP    192.168.1.50:61559     a2-16-210-111:https    CLOSE_WAIT
  TCP    192.168.1.50:61621     wm-in-f188:5228        ESTABLISHED
  TCP    192.168.1.50:61896     52.114.75.79:https     ESTABLISHED
  TCP    192.168.1.50:61898     20.140.56.69:https     ESTABLISHED
  TCP    192.168.1.50:61923     ec2-13-113-49-218:https  ESTABLISHED
  TCP    192.168.1.50:62001     162.159.130.234:https  ESTABLISHED
  TCP    192.168.1.50:62438     ns3152275:17301        ESTABLISHED
  TCP    192.168.1.50:62576     151.101.122.132:https  ESTABLISHED
  TCP    192.168.1.50:62674     162.159.134.232:https  ESTABLISHED
  TCP    192.168.1.50:62692     162.159.133.233:https  ESTABLISHED
  TCP    192.168.1.50:62708     text-lb:https          TIME_WAIT
  TCP    192.168.1.50:62712     104.28.0.30:https      ESTABLISHED
  TCP    192.168.1.50:62715     104.28.0.30:https      ESTABLISHED
  TCP    192.168.1.50:62721     104.16.221.29:https    ESTABLISHED
  TCP    192.168.1.50:62723     fra15s10-in-f66:https  ESTABLISHED
  TCP    192.168.1.50:62725     server-13-249-33-23:https  ESTABLISHED
  TCP    192.168.1.50:62728     151.101.2.8:https      ESTABLISHED
  TCP    192.168.1.50:62731     getclicky:https        ESTABLISHED
  TCP    192.168.1.50:62733     1:https                ESTABLISHED
  TCP    192.168.1.50:62738     ec2-52-72-138-62:https  CLOSE_WAIT
  TCP    192.168.1.50:62739     par10s29-in-f227:https  TIME_WAIT
  TCP    192.168.1.50:62740     par10s29-in-f227:https  TIME_WAIT
  TCP    192.168.1.50:62743     a2-19-228-118:https    ESTABLISHED
  TCP    192.168.1.50:62748     a2-21-210-214:https    ESTABLISHED
  TCP    192.168.1.50:62749     a2-21-210-214:https    ESTABLISHED
  TCP    192.168.1.50:62750     ec2-34-196-158-99:https  CLOSE_WAIT
  TCP    192.168.1.50:62755     server-13-249-11-69:https  ESTABLISHED
  TCP    192.168.1.50:62758     104.19.184.2:https     ESTABLISHED
  TCP    192.168.1.50:62761     49:https               ESTABLISHED
  TCP    192.168.1.50:62763     49:https               TIME_WAIT
  TCP    192.168.1.50:62770     ad9411418cf2cdacd:https  ESTABLISHED
  TCP    192.168.1.50:62774     49:https               ESTABLISHED
  TCP    192.168.1.50:62776     151.101.122.133:https  ESTABLISHED
  TCP    192.168.1.50:62781     server-13-249-11-69:https  ESTABLISHED
  TCP    192.168.1.50:62782     server-13-249-11-69:https  TIME_WAIT
  TCP    192.168.1.50:62783     a2-20-58-56:https      ESTABLISHED
  TCP    192.168.1.50:62784     a2-20-58-56:https      ESTABLISHED
  TCP    192.168.1.50:62785     151.101.120.84:https   ESTABLISHED
  TCP    192.168.1.50:62797     ec2-3-213-18-157:https  ESTABLISHED
  TCP    192.168.1.50:62799     52.109.8.20:https      ESTABLISHED
  TCP    192.168.1.50:62800     ec2-52-44-114-230:https  ESTABLISHED
  TCP    192.168.1.50:62801     tif-bs:https           ESTABLISHED
  TCP    192.168.1.50:62802     ionos:https            ESTABLISHED
  TCP    192.168.1.50:62803     a2-16-209-55:https     ESTABLISHED
  TCP    192.168.1.50:62805     t-bs:https             ESTABLISHED
  TCP    192.168.1.50:62806     t-bs:https             ESTABLISHED
```
netstat -a -b 
