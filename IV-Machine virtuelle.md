# IV-Machine virtuelle

Je créé un dossier d sur mon bureau et j'ajoute un fichier test.txt dans lequel j'ecris "Salut!"
clique droit sur le dossier d > propriétés > partage > partage avancer > je nomme le nom du partage d
Je lance un powershell

```
PS C:\Users\adrie> ssh root@192.168.120.50
root@192.168.120.50's password:
Last login: Mon Nov  9 18:00:25 2020 from 192.168.120.1
```
On execute les commandes : 
```
yum install -y cifs-utils
mkdir /opt/partage
mount -t cifs -o username=<VOTRE_UTILISATEUR>,password=<VOTRE_MOT_DE_PASSE> //<IP_DE_VOTRE_PC>/<NOM_DU_PARTAGE> /opt/partage
```
Ici <NOM_DU_PARTAGE> est d 
```
[root@localhost ~]# cd /opt/partage/
[root@localhost partage]# ls
test.txt
[root@localhost partage]# cat test.txt
Salut!
```